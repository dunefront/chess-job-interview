import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'df-chess-controls',
  templateUrl: './chess-controls.component.html',
  styleUrls: ['./chess-controls.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChessControlsComponent {
  public moveFrom = '';
  public moveTo = '';
}
