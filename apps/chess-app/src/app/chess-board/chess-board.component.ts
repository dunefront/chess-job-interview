import { ChangeDetectionStrategy, Component, ElementRef, QueryList, ViewChildren } from '@angular/core';
import { TimerServiceService } from '../services/timer-service.service';

@Component({
  selector: 'df-chess-board',
  templateUrl: './chess-board.component.html',
  styleUrls: ['./chess-board.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChessBoardComponent {

  constructor(private timer: TimerServiceService) {
  }

  public cols = 'a b c d e f g h'.split(' ');
  public rows = '8 7 6 5 4 3 2 1'.split(' ');

  public time = 0;

  @ViewChildren('square', { read: ElementRef })
  public squaresElementRefs!: QueryList<ElementRef>;

  public getClasses(col: string, row: number): string[] {
    const classes = this.getBackgroundClasses(col, row);
    if (row == 8) {
      if (col == 'b' || col == 'g')
        classes.push('knight-0');
    }
    if (row == 1) {
      if (col == 'b' || col == 'g')
        classes.push('knight-1');
    }
    return classes;
  }

  private getBackgroundClasses(col: string, rowIndex: number): string[] {
    const classes = [];
    classes.push('board');
    classes.push('square');
    const colIndex = this.getColIndex(col);

    if (rowIndex % 2 == 0)
      classes.push(colIndex % 2 == 0 ? 'light' : 'dark');
    else
      classes.push(colIndex % 2 == 0 ? 'dark' : 'light');
    return classes;
  }

  public move() {
    this.timer.start();
    this.clearSquare(1);
    this.setKnightInSquare(18, 0);
  }

  private getColIndex(c: string) {
    return c.charCodeAt(0) - 'a'.charCodeAt(0);
  }

  private setKnightInSquare(i: number, bw: number) {
    const squares = this.squaresElementRefs.toArray();
    const classList = squares[i].nativeElement.classList;
    classList.remove('knight-0');
    classList.remove('knight-1');
    classList.remove('clear');
    classList.add(`knight-${bw}`);
    let classNames = '';
    classList.forEach((c: string) => classNames += `${c} `);
    squares[i].nativeElement.classNames = classNames;
  }

  private clearSquare(i: number) {
    const squares = this.squaresElementRefs.toArray();
    const classList = squares[i].nativeElement.classList;
    classList.remove('knight-0');
    classList.remove('knight-1');
    classList.add('clear');
    let classNames = '';
    classList.forEach((c: string) => classNames += `${c} `);
    squares[i].nativeElement.classNames = classNames;
  }
}
