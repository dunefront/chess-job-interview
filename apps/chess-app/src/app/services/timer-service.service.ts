import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimerServiceService {
  public timer = new BehaviorSubject<number>(0);

  public start(): void {
    let counter = 0;
    setInterval(() => {
      this.timer.next(counter++);
      console.log("Timer: " + counter);
    }, 1000);
  }
}
